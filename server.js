const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const dotenv = require('dotenv')
const PORT = 3000 || process.env.PORT;
const log = require('./api/middlewear/log')
dotenv.config();

//import Routes
const carRoutes = require('./api/routes/brands');
const usedCarRoutes = require('./api/routes/used_cars');
const userRoutes = require('./api/routes/authUser');

app.use(log);
app.use('/uploads',express.static('uploads'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/brands',carRoutes);
app.use('/usedCars',usedCarRoutes);
app.use('/user',userRoutes)








//connect db
mongoose.connect(process.env.MONGODB_URL || 'mongodb://localhost/carventure',{useNewUrlParser: true,useUnifiedTopology: true});
let db = mongoose.connection;

// Check connection
db.once('open', function(){
    console.log('Connected to MongoDB');
  });


// Check for DB errors
db.on('error', function(err){
    console.log(err);
  });


//URL+ not found 
app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});



app.listen(PORT,function(){
    console.log(`SERVER IS RUNNING AT ${PORT}`)
})

