const mongoose = require('mongoose');
//schema
const usedCarsSchema = mongoose.Schema({ 
    brand_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'CarBrands',
        required: true
    },
    reg_no:{
        type: String,
        required: true,     
        unique:true

    },
    city:{
        type: String,
        required: true

    },
    car_image:{
        type: String,
        required: true
    },
    model:{
        type: String,
        required: true
    },
    type:{
        type: String,
        required: true
    },
    fuel_type:{
        type: String,
        required: true
    },
    color:{
        type: String,
        required: true
        },
    gear_type:{
        type: String,
        required: true
    },
    km_driven:{
        type: Number,
        required: true
    },
    selling_price:{
        type: Number,
        required: true
}

},{ timestamps: {  }})
 
 module.exports = mongoose.model('UsedCars',usedCarsSchema);