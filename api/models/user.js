const mongoose = require("mongoose");


const UserSchema = mongoose.Schema({
  fullName: {
    type: String,
    required: true,

  },
  phone_number: {
    type: Number,        
    required: true,
  },
  email: {
    type: String,
    required: true, 
    unique:true,
    lowercase:true,
  },
  password:{
    type:String,
    required: true,


  }
},{ timestamps: {}});

module.exports = mongoose.model('user', UserSchema);