const mongoose = require('mongoose');
//schema
const CarSchema = mongoose.Schema({ 
   brand_name:{
      type: String,
      required: true,
      unique: true,
      lowercase: true,
      

   }
},{ timestamps: { createdAt: 'created_at' } })
 
 module.exports = mongoose.model('CarBrands',CarSchema);