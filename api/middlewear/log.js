const fs = require("fs");
  

 //middlewear function for logging the details
 module.exports= (req, res, next) => { 
    let current_datetime = new Date();
    let method = req.method;
    let url = req.url;
    let status = res.statusCode;

    let log = `[${current_datetime}] ${method}:${url} ${status} } `;
    console.log(log);
    fs.appendFile("request_logs.txt", log + "\n", err => {
      if (err) {
        console.log(err);
      }
    });
    next();
  };