const router = require('express').Router();
const UserSchema = require('../models/user')
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')
const {registerValidation, loginValidation} = require('../controllers/validateUser')


//POST register users
router.post('/register',async(req,res)=>{

    const {error} = registerValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message)

//if User exist
    const userExist = await UserSchema.findOne({email:req.body.email})    
    if(userExist) return res.status(400).send('Email already exists')

//hash the password
    const salt = await bcrypt.genSalt(10)
    const hashedPassword = await bcrypt.hash(req.body.password, salt)


    const user =  new UserSchema({
        fullName: req.body.fullName,
        phone_number: req.body.phone_number,
        email: req.body.email,
        password: hashedPassword

    });
    try{
        const savedusers = await user.save()
        res.json(savedusers);

    }catch(err){
        res.status(400).send({message: err})

    }
});

//POST register users
router.post('/login',async(req,res)=>{

    const {error} = loginValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message)

    //if User doesnt exist
    const user = await UserSchema.findOne({email:req.body.email})    
    if(!user) return res.status(400).send('Email or password is wrong')

    //password is correct
    const validPass = await bcrypt.compare(req.body.password,user.password)
    if(!validPass) return res.status(400).send('Email or password is wrong')

    //create and assign a token
    const token = jwt.sign({_id: user._id },process.env.TOKEN_SECRET)
    res.header('auth-token',token).status(200).send(`${token} loggen in successfully`)
 



});
     







//get all users
router.get('/',async (req,res)=>{
    try{
        const users = await UserSchema.find();
        res.json(users);
    }catch(err){
        res.json({message: err})
    }
})  




//GET a specifc user
router.get('/:id',async (req,res)=>{
    try{
        const users = await UserSchema.findById(req.params.id);
        res.json(users);
    }catch(err){
        res.json({message: err})
    }
})  

//Delete a user
router.delete('/:id',async (req,res)=>{
    try{
        const removeUser = await UserSchema.remove({  _id:req.params.id});
        res.json(removeUser);
    }catch(err){
        res.json({message: err})
    }
});

// 

module.exports = router ;
