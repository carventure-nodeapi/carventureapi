const express = require('express');
const router =  express.Router();
const CarSchema = require('../models/brands')

//GET all car brands
router.get('/',async (req,res)=>{
    try{
        const { page = 1, limit = 10 } = req.query;
        const count = await CarSchema.countDocuments();
        const brands = await CarSchema.find()
            .limit(limit * 1)
            .skip((page -1) * limit)
        res.status(200).json({limit :brands.length,count: count,brands,totalPages: Math.ceil(count / limit),currentPage: page});
    }catch(err){
        res.status(500).json({message: err})
    }
});


//GET a specifc brand
router.get('/:brandname',async (req,res)=>{
    try{
        const brand = await CarSchema.find({brand_name:req.params.brandname});
        res.json(brand);
    }catch(err){
        res.json({message: err})
    }

});

//POST a  car brand
router.post('/', async   (req,res)=>{
    const cars = new CarSchema({
        brand_name:req.body.brand_name
    });
    try{
        const savedcars = await cars.save()
        res.json(savedcars);
    }
    catch(err){
        res.json({message: err})
    }
});


//DELETE a specific brand
router.delete('/:brand_id',async (req,res)=>{
    try{
        const removeBrand = await CarSchema.remove({  _id:req.params.brand_id});
        res.json(removeBrand);
    }catch(err){
        res.json({message: err})
    }
});

//UPDATE a specific brand
router.patch('/:brand_id',async (req,res)=>{
    try{
        const updateBrand = await CarSchema.updateOne(
            {_id:req.params.brand_id},
            {$set:{brand_name:req.body.brand_name}
        });
            res.json(updateBrand);
    }catch(err){
        res.json({message: err})
    }
});




module.exports = router ;