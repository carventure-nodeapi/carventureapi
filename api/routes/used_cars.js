const express = require('express');
const router =  express.Router();
const usedCarsSchema = require('../models/used_carsModel')
const multer = require('multer');
//const controllers = require('../controllers/usedcarsC')
const verify = require('../middlewear/verifyToken')


const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null,'./uploads/');
    },
    filename: function(req,file, cb){
        cb(null,new Date().toISOString().replace(':', '-') );
    }
});
const fileFilter = (req,file,cb)=>{
    //reject a file
    if(file.mimetype ==='image/jpeg' ||file.mimetype ==='image/png'){
    cb(null, true);  
    }
    else{
        cb(null,false)
    }
    
}

const upload = multer({
    storage: storage,
     limits: {
        fileSize: 1024 * 1024 * 5
    } ,
    fileFilter: fileFilter
});


//GET all used cars
router.get('/', async (req,res)=>{
    try{
        const { page = 1, limit = 10 } = req.query;
        const count = await usedCarsSchema.countDocuments();
        const usedcars = await usedCarsSchema.aggregate([
            {$lookup:
                {
                    from: 'carbrands',
                    localField:'brand_id',
                    foreignField:'_id',
                    as:"BRAND DETAILS"
                }

            }
            // ,{
            //     $project:{
            //         _id:1,
            //         brand_name:1

            //     }
            // }
            
        ])         
        //res.json({limit :brands.length,count: count,usedcars,totalPages: Math.ceil(count / limit),currentPage: page})
        res.json({limit :usedcars.length,count: count,usedcars,totalPages: Math.ceil(count / limit),currentPage: page})
    }catch(err){
        res.json({message: err})
    }

})    

//POST uses cars
router.post('/',upload.single('car_image'),verify, async (req,res)=>{
   
    if (!req.file) return res.send('Please post required attributes')
    

    const usedcars = new usedCarsSchema({  
        brand_id: req.body.brand_id,
        reg_no: req.body.reg_no,
        city: req.body.city,
        car_image: req.file.path,
        model: req.body.model,
        type: req.body.type,
        fuel_type: req.body.fuel_type,
        color: req.body.color,
        gear_type:req.body.gear_type,
        km_driven:req.body.km_driven,
        selling_price: req.body.selling_price

    });
    try{
        const savedcars = await usedcars.save()
        res.json(savedcars);
    }
    catch(err){
        res.json({message: err})
    }
});


//get a specifc car
router.get('/:id',async (req,res)=>{
    try{
        const cars = await usedCarsSchema.findById(req.params.id);
        res.json(cars);
    }catch(err){
        res.json({message: err})
    }
})    

//DELETE a sepecific car
router.delete('/:id',async (req,res)=>{
    try{
        const removeCar = await usedCarsSchema.remove({  _id:req.params.id});
        res.json(removeCar);
    }catch(err){
        res.json({message: err})
    }
});


//UPDATE  all
router.patch('/:id',async (req,res)=>{
    try{
        const updateAll = await usedCarsSchema.updateMany(
            {_id:req.params.id},
            {$set:{city: req.body.city},
    });
            res.json(updateAll);
    }catch(err){
        res.json({message: err})
    }
});


//get all users
router.get('/search',async (req,res)=>{
    try{
        const users = await usedCarsSchema.find();
        res.status(200).json(users);  
    }catch(err){
        res.status(400).json({message: 'not happenning'})
    }
})  


   



module.exports = router ;